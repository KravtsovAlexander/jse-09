package ru.t1.kravtsov.tm.controller;

import ru.t1.kravtsov.tm.api.controller.IProjectTaskController;
import ru.t1.kravtsov.tm.api.service.IProjectTaskService;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectID = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskID = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskToProject(projectID, taskID);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void unbindTaskToProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectID = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskID = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectID, taskID);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

}
