package ru.t1.kravtsov.tm.api.service;

import ru.t1.kravtsov.tm.api.repository.ICommandRepository;
import ru.t1.kravtsov.tm.model.Command;

public interface ICommandService extends ICommandRepository {

}
