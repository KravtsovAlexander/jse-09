package ru.t1.kravtsov.tm.api.controller;

public interface ITaskController {

    void displayTasks();

    void createTask();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTasksByName();

    void displayTaskById();

    void displayTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void displayTasksByProjectId();

}
