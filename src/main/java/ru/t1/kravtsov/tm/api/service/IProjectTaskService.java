package ru.t1.kravtsov.tm.api.service;

import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    Task unbindTaskFromProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    void removeProjects(List<Project> projects);

}
